var key = 'a0a816ae7dd9a002016b1a004ece3ca2';
var token = '309a8e15f0fa654f43bf9d8997c7c9c207be31e78d83a23aadf32522a171e3d5';
var boardId = '5d89992d2883bf7ba76e36d4';
var cardId = '5d899cb950249a77521a5157';

async function fetchingMethod(url, fetchMethod) {
  let response = await fetch(url, { method: fetchMethod });
  let data = await response.json();
  console.log(data);
  return data;
};

function checkListGetMethod() {
  let url = `https://api.trello.com/1/cards/${cardId}/checklists?&key=${key}&token=${token}`;
  return fetchingMethod(url, "GET");
};

function checkListPostMethod(checkListName) {
  let url = `https://api.trello.com/1/cards/${cardId}/checklists?&name=${checkListName}&key=${key}&token=${token}`;
  return fetchingMethod(url, "POST");
};

function checkListDeleteMethod(checkListId) {
  let url = `https://api.trello.com/1/cards/${cardId}/checklists/${checkListId}?&key=${key}&token=${token}`;
  return fetchingMethod(url, "DELETE");
};

function checkListEditMethod(checkListID, rename) {
  let url = `https://api.trello.com/1/checklists/${checkListID}/name?value=${rename}&key=${key}&token=${token}`;
  return fetchingMethod(url, "PUT");
}

function checkListView(data) {
  let listData = createCheckList(data);
  $("#List").append(listData);
  $("#List").css({ marginLeft: "300px", marginRight: "300px", marginTop: "50px", marginBottom: "50px" });
  CheckList(listData, data);
}

function createCheckList(list) {
  var deleteButton = $('<button>').attr({
    class: "close ",
    "delete": `${list["id"]}`,
  }).append(`<span aria-hidden="true">&times;</span>`)
  var CheckLists = $('<div>').attr({
    class: "card",
    "checkListId": `${list["id"]}`,
  })
    .append($('<div>').attr({ class: 'text-center' })
      .append(
        $('<div>')
          .attr({ class: "card-header" }).css("background-color", "green")
          .append(
            (
              $("<span>")
                .attr({
                  class: "header h5",
                  "checkListId": `${list["id"]}`,
                })
                .append(
                  `${list["name"]}`,
                )
            ),
            deleteButton,
          ),
        editListandItems(list["id"], "addItem", "CheckItems", "Add"),
      )
    )
  list["checkItems"].forEach(item => {
    let CheckItems = checkItemViews(item, `${list["id"]}`);
    CheckLists.append(CheckItems);
  });
  deleteList(deleteButton);
  return CheckLists;
}

function deleteList(deleteButton) {
  deleteButton.on('click', () => {
    var deleteCheckListId = deleteButton.attr("delete");
    console.log(deleteCheckListId);
    checkListDeleteMethod(deleteCheckListId).then(() => {
      $(`div[checkListId = ${deleteCheckListId}]`).remove();
      console.log("removed");
    })
  });
}

function CheckList(parentList, list) {
  $(`.addItem[checkListId = ${list["id"]}]`).on('submit', (e) => {
    e.preventDefault();
    var itemName = $(`.newListandItem[checkListId = ${list["id"]}]`).val();
    CheckItemPostMethod(`${list["id"]}`, itemName)
      .then(item => (checkItemViews(item, `${list["id"]}`)))
      .then(child => parentList.append(child));
    $(`.newListandItem[checkListId = ${list["id"]}]`).val('');
  });
  $(`.header[checkListId = "${list["id"]}"]`).on('dblclick', (event) => {
    var value = $(event.target).text();
    var delBtn = $(`button[delete = "${list["id"]}"]`);
    $(event.target).html(editListandItems(list["id"], "change-name", "CheckListName", "Add"));
    var input = $(`.change-name input[checkListId = "${list["id"]}"]`);
    input.val(value);
    delBtn.off('click');
    delBtn.on('click', () => {
      $(event.target).html(value);
      deleteList(delBtn);
    });
    $(`.change-name[checkListId = "${list["id"]}"]`).on('submit', (event) => {
      event.preventDefault();
      checkListEditMethod(`${list["id"]}`, input.val())
        .then(() => {
          $(`.header[checkListId = "${list["id"]}"]`).html(`${input.val()}`);
        });
    });
  });
}


$(function () {
  checkListGetMethod()
    .then(response => {
      response.forEach(data => {
        checkListView(data);
      });
    });

  $("#myForm").on('submit', (e) => {
    e.preventDefault();
    var checkListName = $("#text").val();
    checkListPostMethod(checkListName)
      .then(list => (checkListView(list)));
    $("#text").val('');
  });
});


function editListandItems(listId, classes, placeholder, button) {
  return `<div class="card-body">
    <form class="${classes}" checkListId=${listId}>
      <div class="input-group">
        <input
          class="form-control newListandItem"
          checkListId = ${listId}
          type="text"
          placeholder=${placeholder}
          autocomplete="off"/>
        <span class="input-group-btn">
          <button class="btn btn-info" type="submit">${button}</button>
        </span>
      </div>
  </form>
  </div>`;
};

function CheckItemPostMethod(checkListId, checkItemName) {
  let url = `https://api.trello.com/1/checklists/${checkListId}/checkItems?&name=${checkItemName}&pos=bottom&checked=false&key=${key}&token=${token}`;
  return fetchingMethod(url, "POST");
};

function CheckItemDelteMethod(checkListId, checkItemId) {
  let url = `https://api.trello.com/1/checklists/${checkListId}/checkItems/${checkItemId}?&key=${key}&token=${token}`;
  return fetchingMethod(url, "DELETE");
}

function CheckItemsEditMethod(checkItemId, newName) {
  let url = `https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?name=${newName}&key=${key}&token=${token}`;
  return fetchingMethod(url, "PUT");
}

function CheckItemStatus(checkItemId, cStatus) {
  let url = `https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?state=${cStatus}&key=${key}&token=${token}`;
  return fetchingMethod(url, "PUT");
}

function checkItemViews(item, checkListID) {
  let child = createCheckItems(item, checkListID);
  return child;
}

function checkItemdelete(btn, listID, itemID) {
  btn.on('click', () => {
    CheckItemDelteMethod(listID, itemID).then(() => {
      $(`div[itemId = ${itemID}]`).hide("slow", () => { $(this).remove(); });
    });
  });
}

function checkItems(statusButton, nameSpan, itemDeleteButton, item, checkListID) {
  nameSpan.on('dblclick', (event) => {
    var itemid = $(event.target).attr("data-listitemid");
    var value = $(event.target).text();
    $(event.target).html(editListandItems(itemid, "change-item-name", "ItemsName", "Add"));
    var input = $(`.edit[data-listitemid = "${itemid}"] input[checkListId = "${itemid}"]`);
    input.val(value);
    itemDeleteButton.off('click');
    itemDeleteButton.on('click', () => {
      $(event.target).html(value);
      checkItemdelete(itemDeleteButton, checkListID, item["id"]);
    });
    $(`.change-item-name[checkListId = "${itemid}"]`).on('submit', (event) => {
      event.preventDefault();
      CheckItemsEditMethod(itemid, input.val())
        .then(() => {
          nameSpan.html(`${input.val()}`);
        });
    });
  });

  statusButton.on('click', () => {
    if (statusButton.attr("data-status") == "complete") {
      CheckItemStatus(statusButton.attr("data-listitemid"), "incomplete")
        .then(() => {
          nameSpan.css({
            "text-decoration": "none",
          });
          statusButton.removeClass("btn-outline-success");
          statusButton.addClass("btn-outline-danger").val(" ");
          statusButton.attr("data-status", "incomplete");
        });
    } else {
      CheckItemStatus(statusButton.attr("data-listitemid"), "complete")
        .then(() => {
          nameSpan.css({
            "text-decoration": "line-through",
            "text-decoration-color": "green",
            // "border-bottom": '0.125em solid blue',
          });
          statusButton.removeClass("btn-outline-danger");
          statusButton.addClass("btn-outline-success").val(`✔`);
          statusButton.attr("data-status", "complete");
        });
    }
  });
  checkItemdelete(itemDeleteButton, checkListID, item["id"]);
}

function createCheckItems(item, checkListID) {
  let statusButton = $("<input>")
    .attr({
      class: "status btn btn-sm",
      type: "button",
      "data-listitemid": `${item["id"]}`,
      "data-status": `${item["state"]}`,
    });
    // $('.btn-sum').css({marginLeft:"1em"})
  let nameSpan = $("<span>")
    .attr({
      class: "edit d-flex align-items-md-center",
      "checkListId": `${checkListID}`,
      "data-listitemid": `${item["id"]}`,
    }).css({marginLeft:"1em"})
    .append(`${item["name"]}`);
  let itemDeleteButton = $('<button>').attr({
    class: "close",
    "delete": `${item["id"]}`,
  }).append(`<span aria-hidden="true">&times;</span>`);
  var child = $('<div>')
    .attr({
      class: "card",
      "itemId": `${item["id"]}`,
    })
    .append(
      $("<div>")
        .attr({
          class: "card-body d-flex justify-content-start",
        })
        .append(statusButton, nameSpan)
        .append($("<div>")
          .attr({
            class: "card-body ml-auto p-2",
          }).append(itemDeleteButton)
        ))

  if (item["state"] == "complete") {
    nameSpan.css({
      "text-decoration": "line-through",
      "text-decoration-color": "green",
    });
    statusButton.addClass("btn-outline-success").val(`✔`)
  } else {
    statusButton.addClass("btn-outline-danger").val(" ");
  }
  checkItems(statusButton, nameSpan, itemDeleteButton, item, checkListID);
  return child;
};
