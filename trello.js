var key = 'a0a816ae7dd9a002016b1a004ece3ca2';
var token = '309a8e15f0fa654f43bf9d8997c7c9c207be31e78d83a23aadf32522a171e3d5';
var boardId = '5d89992d2883bf7ba76e36d4';
var listid = '5d899cab5827ec60140f7853';
var cardid = '5d899cb950249a77521a5157';
var list_id, checklistIds = [];

//Get CheckList;
function FetchingtrelloCheckList() {
  fetch(`https://api.trello.com/1/cards/${cardid}/checklists?key=${key}&token=${token}`)
    .then(function (list) {
      return list.json();
    }).then(function (json) {
      console.log(json);
      list_id = json;

      for (let i = 0; i < json.length; i++) {
        let result = json[i].name;
        listId = json[i].id;
        //console.log(list_id[i].id);
        checklistIds[i] = list_id[i].id;

        $(".list").append(` <div class="text-center" id="firstdiv"><div class="card" id=${listId}>
            <div class="card-header">${result}
            
             <button type="button" class="close" onclick=deleteCheckList('${listId}','${key}','${token}') aria-label="Close">

            <span aria-hidden="true">&times;</span></button>
            <button type="button" class="close" onclick=EditCheckList('${listId}','${key}','${token}') aria-label="Edit">

            <span aria-hidden="true">&hellip;</span></button>

            </div>
            <div class="card-body">
               <input type="text" id ="input" placeholder="Item">
              <button type="button" id="addItem" class="btn btn-primary" onclick=addCheckItems('${listId}','${key}','${token}')>Add Items</button>    
              <button type="button" id="delete" class="btn btn-primary">Delete Items</button>
            </div>
            </div>
          </div>`);
        $('card-header').val('');
        FetchingtrelloCheckItems(list_id[i].id);
        // addCheckItem(list_id[i.id]);
      }
      // document.getElementById('#firstdiv').addEventListener('click',deleteCheckList)
    })
}
//GET CheckItems.

function FetchingtrelloCheckItems(id) {
  fetch(`https://api.trello.com/1/checklists/${id}/checkItems?token=${token}&key=${key}`)
    .then(function (data) {
      return data.json();     //converting into Json data.
    }).then(function (myjson) {
      for (let i = 0; i < myjson.length; i++) {
        //console.log(id, myjson[i].name);
        let item = myjson[i].name;
        $(`[id=${id}]`).append(`
             <ul>
             <li type="none"><input type="checkbox">${item}</li>
             </ul>
             `)
      }
    })

}

let userInput;
$("#myForm").on('submit', function (e) {
  e.preventDefault();
  userInput = $('#text').val();
  $(".list").append(` <div class="text-center" ><div class="card" id="newCard">
      <div class="card-header">${userInput}
      <button type="button" class="close"  aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
      <button type="button" class="close"  aria-label="Edit">
              <span aria-hidden="true">&hellip;</span></button>        
      </div>
      <div class="card-body">
         <input type="text" id ="input" placeholder="Item">
        <button type="button" id="addItem" class="btn btn-primary">Add Items</button>    
        <button type="button" id="delete" class="btn btn-primary">Delete Items</button>
      </div></div>
    </div>`);
  $('#text').val('');

  addCheckList();
})

function addCheckList() {
  // var name = document.getElementById('#text').nodeValue;
  fetch(`https://api.trello.com/1/cards/${cardid}/checklists?checkItems=all&checkItem_fields=
   name%2CnameData%2Cpos%2Cstate&filter=all&fields=all&key=${key}&token=${token}&name=${userInput}`, {
    method: 'Post',
  }).then((response) => response.json())
    .then(() => function (data) {
      console.log(data.id);

    })
    .catch(error => console.log(error));
}

function deleteCheckList(idList, key, token) {
  // console.log('delete',idList);
  fetch(`https://api.trello.com/1/cards/5d899cb950249a77521a5157/checklists/${idList}?key=${key}&token=${token}`, {
    method: 'DELETE'
  }).then(() => {
    console.log('removed');
    //location.reload();
  })
    .catch(error => console.log(error));
}



// function EditCheckList(idList, key, token)
// {
//   let name;
//   $("myform").on('submit', function (e) {
//     e.preventDefault();
//     name = $('#texts').val();
//     $(`[id=${id}]`).append(`<form class="form-inline" onsubmit="openModal()" id="myForm">
//     <button type="button" class="close"  aria-label="Edit">
//     <span aria-hidden="true">&hellip;</span></button> 
//   </form>

//     <div class="modal fade" tabindex="-1" role="dialog" id="myModal">
//     <div class="modal-dialog" role="document">
//       <div class="modal-content">
//         <div class="modal-header">
//           <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

//         </div>
//         <div class="modal-body">
//            <input type="texts" placeholder="checkList">

//         </div>
//         <div class="modal-footer">
//           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
//           <button type="button" class="btn btn-primary">Save changes</button>
//         </div>
//       </div>
//     </div>
//   </div>`)
//   })

//   fetch(`https://api.trello.com/1/checklists/${idList}/name?value=${name}&${key}&${token}`,{
//     method:'PUT'
//   }).then((response) => response.json())
//   .then(() => function(data){
//     console.log(data);
//   })
// }

FetchingtrelloCheckList();

var name;
function CheckItems(id){
 $('.card-body').on('click', function (e) {
  e.preventDefault();
   name = $('#input').val();
  console.log(name);

  $(`[id=${id}]`).append(`
  <ul>
  <li type="none"><input type="checkbox">${name}</li>
  </ul>
  `)
})
return name;
}

function addCheckItems(id, key, token) {

  console.log('id;-', id);

  var Name = CheckItems(id);
  console.log(Name);
  fetch(`https://api.trello.com/1/checklists/${id}/checkItems?name=${Name}&key=${key}&token=${token}`, {
    method: 'Post'
  })
    .then((response) => response.json())
    .then(() => function (data) {
      console.log(data);
    })
  
}